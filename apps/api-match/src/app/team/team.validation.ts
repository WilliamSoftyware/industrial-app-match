import { IsObjectId } from '@app-match/api/validation/id';
import {
  TeamCreateDto,
  TeamDto,
  TeamResetDto,
  TeamUpdateDto,
} from '@app-match/common/resource/team';
import {
  IntersectionType,
  OmitType,
  PartialType,
  PickType,
} from '@nestjs/mapped-types';
import { IsString } from 'class-validator';

export class TeamValidationDto implements TeamDto {
  @IsObjectId() id: string;
  @IsString() name: string;
}

export class TeamCreateValidationDto
  extends OmitType(TeamValidationDto, ['id'])
  implements TeamCreateDto {}
export class TeamUpdateValidationDto
  extends IntersectionType(
    PickType(TeamValidationDto, ['id']),
    PartialType(TeamCreateValidationDto)
  )
  implements TeamUpdateDto {}
export class TeamResetValidationDto
  extends TeamUpdateValidationDto
  implements TeamResetDto {}
