import {
  TeamCreateDto,
  TeamDto,
  TeamResetDto,
  TeamUpdateDto,
} from '@app-match/common/resource/team';
import {
  ApiProperty,
  IntersectionType,
  OmitType,
  PartialType,
  PickType,
} from '@nestjs/swagger';

export const teamExample: TeamDto = {
  id: '6214c0f2857cfb3569c19166',
  name: `Paris Saint-Germain`,
};

export class ApiTeamDto implements TeamDto {
  @ApiProperty({ example: teamExample.id }) id: string;
  @ApiProperty({ example: teamExample.name }) name: string;
}

export class ApiTeamCreateDto
  extends OmitType(ApiTeamDto, ['id'])
  implements TeamCreateDto {}
export class ApiTeamUpdateDto
  extends IntersectionType(
    PickType(ApiTeamDto, ['id']),
    PartialType(ApiTeamCreateDto)
  )
  implements TeamUpdateDto {}
export class ApiTeamResetDto extends ApiTeamUpdateDto implements TeamResetDto {}
