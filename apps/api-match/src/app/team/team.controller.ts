import { IsObjectIdPipe } from '@app-match/api/validation/id';
import { resourceTeamPath, TeamDto } from '@app-match/common/resource/team';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
} from '@nestjs/swagger';
import {
  ApiTeamCreateDto,
  ApiTeamDto,
  ApiTeamResetDto,
  ApiTeamUpdateDto,
  teamExample,
} from './team.documentation';
import { TeamService } from './team.service';
import {
  TeamCreateValidationDto,
  TeamResetValidationDto,
  TeamUpdateValidationDto,
} from './team.validation';

@Controller(resourceTeamPath)
export class TeamController {
  constructor(private readonly teamService: TeamService) {}

  @Post()
  @ApiBody({ type: ApiTeamCreateDto })
  @ApiCreatedResponse({ type: ApiTeamDto })
  @ApiBadRequestResponse()
  create(@Body() dto: TeamCreateValidationDto): Promise<TeamDto> {
    return this.teamService.create(dto);
  }

  @Get()
  @ApiOkResponse({ type: [ApiTeamDto] })
  findAll(): Promise<TeamDto[]> {
    return this.teamService.findAll();
  }

  @Get(':id')
  @ApiParam({ name: 'id', example: teamExample.id })
  @ApiOkResponse({ type: ApiTeamDto })
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  findOne(@Param('id', IsObjectIdPipe) id: string): Promise<TeamDto> {
    return this.teamService.findOne(id);
  }

  @Patch(':id')
  @ApiParam({ name: 'id', example: teamExample.id })
  @ApiBody({ type: ApiTeamUpdateDto })
  @ApiOkResponse({ type: ApiTeamDto })
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  update(
    @Param('id', IsObjectIdPipe) id: string,
    @Body() dto: TeamUpdateValidationDto
  ): Promise<TeamDto> {
    return this.teamService.update({ ...dto, id });
  }

  @Put(':id')
  @ApiParam({ name: 'id', example: teamExample.id })
  @ApiBody({ type: ApiTeamResetDto })
  @ApiOkResponse({ type: ApiTeamDto })
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  reset(
    @Param('id', IsObjectIdPipe) id: string,
    @Body() dto: TeamResetValidationDto
  ): Promise<TeamDto> {
    return this.teamService.reset({ ...dto, id });
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiParam({ name: 'id', example: teamExample.id })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  remove(@Param('id', IsObjectIdPipe) id: string) {
    return this.teamService.remove(id);
  }
}
