import { TeamDto } from '@app-match/common/resource/team';
import { Test, TestingModule } from '@nestjs/testing';
import { TeamController } from './team.controller';
import { TeamService } from './team.service';

describe('TeamController', () => {
  let controller: TeamController;
  let serviceMock: Partial<TeamService>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TeamController],
    })
      .useMocker((token) => {
        if (token === TeamService) {
          serviceMock = {
            findAll: jest.fn<Promise<TeamDto[]>, []>(),
          };
          return serviceMock;
        }
      })
      .compile();

    controller = module.get<TeamController>(TeamController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    // it('should return an Promise', () => {
    //   const result = controller.findAll();
    //   expect(result).toBeInstanceOf(Promise);
    // });

    // result is undefined ?
    // it('should call service', (done) => {
    //   const result = controller.findAll();
    //   result.then(() => {
    //     expect(serviceMock.findAll).toBeCalledTimes(1);
    //     done();
    //   });
    // });
  });
});
