import {
  TeamCreateDto,
  TeamDto,
  TeamResetDto,
  TeamUpdateDto,
} from '@app-match/common/resource/team';
import { Logger } from '@nestjs/common';
import { TeamDocument, TeamEntity, TeamEntityWithId } from './team.entity';

const logger = new Logger('TeamMapper');

export const teamDocumentToDto = (document: TeamDocument): TeamDto => {
  logger.debug('start');
  return {
    id: document.id,
    name: document.name,
  };
};

export const teamCreateDtoToEntity = (dto: TeamCreateDto): TeamEntity => {
  logger.debug('start');
  return {
    name: dto.name,
  };
};

export const teamResetDtoToEntity = (dto: TeamResetDto): TeamEntityWithId => {
  logger.debug('start');
  return {
    id: dto.id,
    name: dto.name,
  };
};

export const teamUpdateDtoToEntity = (dto: TeamUpdateDto): TeamEntityWithId => {
  logger.debug('start');
  return {
    id: dto.id,
    name: dto.name,
  };
};
