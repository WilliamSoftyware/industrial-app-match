import { handleDocumentNotFound } from '@app-match/api/repository/error';
import {
  TeamCreateDto,
  TeamDto,
  TeamResetDto,
  TeamUpdateDto,
} from '@app-match/common/resource/team';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { TeamDocument, TeamEntity } from './team.entity';
import {
  teamCreateDtoToEntity,
  teamDocumentToDto,
  teamResetDtoToEntity,
  teamUpdateDtoToEntity,
} from './team.mapper';

@Injectable()
export class TeamService {
  constructor(
    @InjectModel(TeamEntity.name) private model: Model<TeamDocument>
  ) {}

  create(dto: TeamCreateDto): Promise<TeamDto> {
    const entity = teamCreateDtoToEntity(dto);
    return this.model.create(entity).then(teamDocumentToDto);
  }

  findAll(): Promise<TeamDto[]> {
    return this.model
      .find()
      .exec()
      .then((entities) => entities.map(teamDocumentToDto));
  }

  findOne(id: string): Promise<TeamDto> {
    return this.model
      .findById(id)
      .orFail()
      .exec()
      .then(teamDocumentToDto)
      .catch(handleDocumentNotFound);
  }

  update(dto: TeamUpdateDto): Promise<TeamDto> {
    const entity = teamUpdateDtoToEntity(dto);
    return this.model
      .findByIdAndUpdate(entity.id, entity, { new: true })
      .orFail()
      .exec()
      .then(teamDocumentToDto)
      .catch(handleDocumentNotFound);
  }

  reset(dto: TeamResetDto): Promise<TeamDto> {
    const entity = teamResetDtoToEntity(dto);
    return this.model
      .findByIdAndUpdate(entity.id, entity, { new: true })
      .orFail()
      .exec()
      .then(teamDocumentToDto)
      .catch(handleDocumentNotFound);
  }

  remove(id: string): Promise<void> {
    return this.model
      .deleteOne({ _id: id })
      .orFail()
      .exec()
      .then(() => null)
      .catch(handleDocumentNotFound);
  }
}
