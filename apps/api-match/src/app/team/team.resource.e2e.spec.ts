import { HttpStatus, INestApplication } from '@nestjs/common';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Model } from 'mongoose';
import * as request from 'supertest';
import { TeamDocument, TeamEntity } from './team.entity';
import { TeamModule } from './team.module';

describe('TeamResource', () => {
  let app: INestApplication;
  let mongoMemoryServer: MongoMemoryServer;

  beforeAll(async () => {
    mongoMemoryServer = await MongoMemoryServer.create();
    const uri = mongoMemoryServer.getUri();

    const moduleRef = await Test.createTestingModule({
      imports: [MongooseModule.forRoot(uri), TeamModule],
    }).compile();

    const model = moduleRef.get<Model<TeamDocument>>(
      getModelToken(TeamEntity.name)
    );
    const entities = [
      { name: 'Barcelone' },
      { name: 'Brésil' },
      { name: 'France' },
    ];
    for (const entity of entities) {
      await model.create(entity);
    }

    app = moduleRef.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('GET /teams', () => {
    it('should respond with data', (done) => {
      request(app.getHttpServer())
        .get('/teams')
        .then((response) => {
          expect(response.status).toBe(HttpStatus.OK);
          expect(Array.isArray(response.body)).toBeTruthy();
        })
        .finally(done);
    });
    it('should respond bad request with bad page', (done) => {
      request(app.getHttpServer())
        .get('/team')
        .then((response) => {
          expect(response).toBeDefined();
          expect(response.status).toBe(HttpStatus.NOT_FOUND);
        })
        .finally(done);
    });
  });
});
