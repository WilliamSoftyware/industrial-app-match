import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import {
  TeamDocument,
  TeamEntity,
  TeamEntityWithId,
  TeamSchema,
} from './team.entity';
import { TeamService } from './team.service';
import { Error, model, Model } from 'mongoose';
import * as mockingoose from 'mockingoose';
import { ApiException } from '@app-match/api/core/error';
import { TeamCreateDto } from '@app-match/common/resource/team';

describe('TeamService', () => {
  let service: TeamService;
  let modelMock: Partial<Model<TeamDocument>>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TeamService],
    })
      .useMocker((token) => {
        if (token === getModelToken(TeamEntity.name)) {
          modelMock = model<TeamDocument>(TeamEntity.name, TeamSchema);
          const entities: TeamEntityWithId[] = [
            {
              name: '',
            },
            {
              name: '',
            },
          ];
          mockingoose(modelMock)
            .toReturn(entities, modelMock.find.name)
            .toReturn(10, modelMock.count.name)
            .toReturn(entities[0], modelMock.findOne.name);
          return modelMock;
        }
      })
      .compile();

    service = module.get<TeamService>(TeamService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an Promise', () => {
      const result = service.findAll();

      expect(result).toBeInstanceOf(Promise);
    });
  });

  describe('findOne', () => {
    it('should return an Promise', () => {
      const id = '6214b2f566b3922d448c42c0';
      const result = service.findOne(id);

      expect(result).toBeInstanceOf(Promise);
    });

    it('should stream object items', (done) => {
      const id = '6214b2f566b3922d448c42c0';
      const result = service.findOne(id);

      result.then((data) => {
        expect(typeof data).toBe('object');
        done();
      });
    });

    it('should throw an ApiResourceException', (done) => {
      const id = '6214b2f566b3922d448c42c0';
      mockingoose(modelMock).toReturn(
        new Error.DocumentNotFoundError(''),
        modelMock.findOne.name
      );

      const result = service.findOne(id);

      result.then(null, (error) => {
        expect(error).toBeInstanceOf(ApiException);
        done();
      });
    });

    it('should throw another error', (done) => {
      const id = '6214b2f566b3922d448c42c0';
      mockingoose(modelMock).toReturn(
        new Error.CastError('', null, ''),
        modelMock.findOne.name
      );

      const result = service.findOne(id);

      result.then(null, (error) => {
        expect(error).not.toBeInstanceOf(ApiException);
        done();
      });
    });
  });

  describe('create', () => {
    it('should return an Promise', () => {
      const team: TeamCreateDto = { name: 'test' };
      const result = service.create(team);

      expect(result).toBeInstanceOf(Promise);
    });

    // it('should return an object', (done) => {
    //   const team: TeamCreateDto = { name: 'test' };
    //   const result = service.create(team);

    //   result.then((data) => {
    //     expect(typeof data).toBe('object');
    //     done();
    //   });
    // });

    it('should throw an ApiResourceException', (done) => {
      const id = '6214b2f566b3922d448c42c0';
      mockingoose(modelMock).toReturn(
        new Error.DocumentNotFoundError(''),
        modelMock.findOne.name
      );

      const result = service.findOne(id);

      result.then(null, (error) => {
        expect(error).toBeInstanceOf(ApiException);
        done();
      });
    });

    it('should throw another error', (done) => {
      const id = '6214b2f566b3922d448c42c0';
      mockingoose(modelMock).toReturn(
        new Error.CastError('', null, ''),
        modelMock.findOne.name
      );

      const result = service.findOne(id);

      result.then(null, (error) => {
        expect(error).not.toBeInstanceOf(ApiException);
        done();
      });
    });
  });
});
