import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ collection: 'Team' })
export class TeamEntity {
  @Prop({ required: true }) name: string;
}

export type TeamEntityWithId = TeamEntity & Pick<Document, 'id'>;
export type TeamDocument = TeamEntity & Document;
export const TeamSchema = SchemaFactory.createForClass(TeamEntity);
