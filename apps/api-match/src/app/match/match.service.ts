import { handleDocumentNotFound } from '@app-match/api/repository/error';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  MatchCreateDto,
  MatchDto,
  MatchResetDto,
  MatchUpdateDto,
} from '@app-match/common/resource/match';
import { MatchDocument, MatchEntity } from './match.entity';
import {
  matchCreateDtoToEntity,
  matchDocumentToDto,
  matchResetDtoToEntity,
  matchUpdateDtoToEntity,
} from './match.mapper';

@Injectable()
export class MatchService {
  constructor(
    @InjectModel(MatchEntity.name) private model: Model<MatchDocument>
  ) {}

  create(dto: MatchCreateDto): Promise<MatchDto> {
    const entity = matchCreateDtoToEntity(dto);
    return this.model.create(entity).then(matchDocumentToDto);
  }

  findAll(): Promise<MatchDto[]> {
    return this.model
      .find()
      .exec()
      .then((entities) => entities.map(matchDocumentToDto));
  }

  findOne(id: string): Promise<MatchDto> {
    return this.model
      .findById(id)
      .orFail()
      .exec()
      .then(matchDocumentToDto)
      .catch(handleDocumentNotFound);
  }

  update(dto: MatchUpdateDto): Promise<MatchDto> {
    const entity = matchUpdateDtoToEntity(dto);
    return this.model
      .findByIdAndUpdate(entity.id, entity, { new: true })
      .orFail()
      .exec()
      .then(matchDocumentToDto)
      .catch(handleDocumentNotFound);
  }
  reset(dto: MatchResetDto): Promise<MatchDto> {
    const entity = matchResetDtoToEntity(dto);
    return this.model
      .findByIdAndUpdate(entity.id, entity, { new: true })
      .orFail()
      .exec()
      .then(matchDocumentToDto)
      .catch(handleDocumentNotFound);
  }

  remove(id: string): Promise<void> {
    return this.model
      .deleteOne({ _id: id })
      .orFail()
      .exec()
      .then(() => null)
      .catch(handleDocumentNotFound);
  }
}
