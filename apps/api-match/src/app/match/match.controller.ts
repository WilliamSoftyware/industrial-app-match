import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
} from '@nestjs/swagger';
import { resourceMatchPath, MatchDto } from '@app-match/common/resource/match';
import { MatchService } from './match.service';
import {
  ApiMatchCreateDto,
  ApiMatchDto,
  ApiMatchResetDto,
  ApiMatchUpdateDto,
  matchExample,
} from './match.documentation';
import { IsObjectIdPipe } from '@app-match/api/validation/id';
import {
  MatchCreateValidationDto,
  MatchResetValidationDto,
  MatchUpdateValidationDto,
} from './match.validation';

@Controller(resourceMatchPath)
export class MatchController {
  constructor(private readonly matchService: MatchService) {}

  @Post()
  @ApiBody({ type: ApiMatchCreateDto })
  @ApiCreatedResponse({ type: ApiMatchDto })
  @ApiBadRequestResponse()
  create(@Body() dto: MatchCreateValidationDto): Promise<MatchDto> {
    return this.matchService.create(dto);
  }

  @Get()
  @ApiOkResponse({ type: [ApiMatchDto] })
  findAll(): Promise<MatchDto[]> {
    return this.matchService.findAll();
  }

  @Get(':id')
  @ApiParam({ name: 'id', example: matchExample.id })
  @ApiOkResponse({ type: ApiMatchDto })
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  findOne(@Param('id', IsObjectIdPipe) id: string): Promise<MatchDto> {
    return this.matchService.findOne(id);
  }

  @Patch(':id')
  @ApiParam({ name: 'id', example: matchExample.id })
  @ApiBody({ type: ApiMatchUpdateDto })
  @ApiOkResponse({ type: ApiMatchDto })
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  update(
    @Param('id', IsObjectIdPipe) id: string,
    @Body() dto: MatchUpdateValidationDto
  ): Promise<MatchDto> {
    return this.matchService.update({ ...dto, id });
  }

  @Put(':id')
  @ApiParam({ name: 'id', example: matchExample.id })
  @ApiBody({ type: ApiMatchResetDto })
  @ApiOkResponse({ type: ApiMatchDto })
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  reset(
    @Param('id', IsObjectIdPipe) id: string,
    @Body() dto: MatchResetValidationDto
  ): Promise<MatchDto> {
    return this.matchService.reset({ ...dto, id });
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiParam({ name: 'id', example: matchExample.id })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  remove(@Param('id', IsObjectIdPipe) id: string) {
    return this.matchService.remove(id);
  }
}
