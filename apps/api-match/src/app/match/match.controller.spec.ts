import { MatchDto } from '@app-match/common/resource/match';
import { Test, TestingModule } from '@nestjs/testing';
import { MatchController } from './match.controller';
import { MatchService } from './match.service';

describe('MatchController', () => {
  let controller: MatchController;
  let serviceMock: Partial<MatchService>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MatchController],
    })
      .useMocker((token) => {
        if (token === MatchService) {
          serviceMock = {
            findAll: jest.fn<Promise<MatchDto[]>, []>(),
          };
          return serviceMock;
        }
      })
      .compile();

    controller = module.get<MatchController>(MatchController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    // it('should return an Promise', () => {
    //   const result = controller.findAll();
    //   expect(result).toBeInstanceOf(Promise);
    // });

    // result is undefined ?
    // it('should call service', (done) => {
    //   const result = controller.findAll();
    //   result.then(() => {
    //     expect(serviceMock.findAll).toBeCalledTimes(1);
    //     done();
    //   });
    // });
  });
});
