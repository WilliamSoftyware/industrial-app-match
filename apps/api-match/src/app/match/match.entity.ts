import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ collection: 'Match' })
export class MatchEntity {
  @Prop({ required: true, type: Date }) date: Date;
  @Prop({ required: true }) homeTeamName: string;
  @Prop({ required: true }) awayTeamName: string;
  @Prop({ required: true, type: Number }) awayTeamScore: number;
  @Prop({ required: true, type: Number }) homeTeamScore: number;
}

export type MatchEntityWithId = MatchEntity & Pick<Document, 'id'>;
export type MatchDocument = MatchEntity & Document;
export const MatchSchema = SchemaFactory.createForClass(MatchEntity);
