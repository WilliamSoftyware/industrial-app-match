import {
  MatchCreateDto,
  MatchDto,
  MatchResetDto,
  MatchUpdateDto,
} from '@app-match/common/resource/match';
import { Logger } from '@nestjs/common';
import { MatchDocument, MatchEntity, MatchEntityWithId } from './match.entity';

const logger = new Logger('MatchMapper');

export const matchDocumentToDto = (document: MatchDocument): MatchDto => {
  logger.debug('start');
  return {
    id: document.id,
    date: document.date?.toISOString(),
    homeTeamName: document.homeTeamName,
    awayTeamName: document.awayTeamName,
    homeTeamScore: document.homeTeamScore,
    awayTeamScore: document.awayTeamScore,
  };
};

export const matchCreateDtoToEntity = (dto: MatchCreateDto): MatchEntity => {
  logger.debug('start');
  return {
    date: dto.date && new Date(dto.date),
    homeTeamName: dto.homeTeamName,
    awayTeamName: dto.awayTeamName,
    homeTeamScore: dto.homeTeamScore,
    awayTeamScore: dto.awayTeamScore,
  };
};

export const matchResetDtoToEntity = (
  dto: MatchResetDto
): MatchEntityWithId => {
  logger.debug('start');
  return {
    id: dto.id,
    date: dto.date ? new Date(dto.date) : null,
    homeTeamName: dto.homeTeamName,
    awayTeamName: dto.awayTeamName,
    homeTeamScore: dto.homeTeamScore,
    awayTeamScore: dto.awayTeamScore,
  };
};

export const matchUpdateDtoToEntity = (
  dto: MatchUpdateDto
): MatchEntityWithId => {
  logger.debug('start');
  return {
    id: dto.id,
    date: dto.date && new Date(dto.date),
    homeTeamName: dto.homeTeamName,
    awayTeamName: dto.awayTeamName,
    homeTeamScore: dto.homeTeamScore,
    awayTeamScore: dto.awayTeamScore,
  };
};
