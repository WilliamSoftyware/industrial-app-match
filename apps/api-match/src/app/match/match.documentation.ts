import {
  MatchCreateDto,
  MatchDto,
  MatchResetDto,
  MatchUpdateDto,
} from '@app-match/common/resource/match';
import {
  ApiProperty,
  IntersectionType,
  OmitType,
  PartialType,
  PickType,
} from '@nestjs/swagger';

export const matchExample: MatchDto = {
  id: '1194c0f2836cfb3569c12366',
  date: new Date(Date.now()).toISOString(),
  homeTeamName: 'Barcelone',
  awayTeamName: 'Real Madrid',
  homeTeamScore: 4,
  awayTeamScore: 1,
};

export class ApiMatchDto implements MatchDto {
  @ApiProperty({ example: matchExample.id }) id: string;
  @ApiProperty({ example: matchExample.date }) date: string;
  @ApiProperty({ example: matchExample.homeTeamName }) homeTeamName: string;
  @ApiProperty({ example: matchExample.awayTeamName }) awayTeamName: string;
  @ApiProperty({ example: matchExample.awayTeamScore }) awayTeamScore: number;
  @ApiProperty({ example: matchExample.homeTeamScore }) homeTeamScore: number;
}

export class ApiMatchCreateDto
  extends OmitType(ApiMatchDto, ['id'])
  implements MatchCreateDto {}
export class ApiMatchUpdateDto
  extends IntersectionType(
    PickType(ApiMatchDto, ['id']),
    PartialType(ApiMatchCreateDto)
  )
  implements MatchUpdateDto {}
export class ApiMatchResetDto
  extends ApiMatchUpdateDto
  implements MatchResetDto {}
