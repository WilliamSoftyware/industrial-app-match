import {
  ApiException,
  ApiResourceTypeNotFoundException,
  ApiUnknownErrorException,
} from '@app-match/api/core/error';
import { ErrorDto } from '@app-match/common/resource/error';
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch()
export class ErrorFilter implements ExceptionFilter {
  protected logger = new Logger(ErrorFilter.name);

  catch(exception: Error, host: ArgumentsHost): void {
    const context = host.switchToHttp();
    const request = context.getRequest<Request>();
    const response = context.getResponse<Response<ErrorDto>>();
    const apiException = new ApiUnknownErrorException();

    response.json({
      timestamp: new Date().toISOString(),
      path: request.url,
      statusCode: apiException.getStatus(),
      code: apiException.options.code,
      message: apiException.options.message,
      details: apiException.options.details,
    });

    this.logger.error(exception?.stack || exception);
  }
}

@Catch(NotFoundException)
export class NotFoundExceptionFilter implements ExceptionFilter {
  protected logger = new Logger(NotFoundExceptionFilter.name);

  catch(exception: NotFoundException, host: ArgumentsHost): void {
    const context = host.switchToHttp();
    const request = context.getRequest<Request>();
    const response = context.getResponse<Response<ErrorDto>>();
    const apiException = new ApiResourceTypeNotFoundException();

    response.json({
      timestamp: new Date().toISOString(),
      path: request.url,
      statusCode: apiException.getStatus(),
      code: apiException.options.code,
      message: apiException.options.message,
      details: apiException.options.details,
    });

    this.logger.error(exception?.stack || exception);
  }
}

@Catch(ApiException)
export class ApiExceptionFilter implements ExceptionFilter {
  protected logger = new Logger(ApiExceptionFilter.name);

  catch(exception: ApiException, host: ArgumentsHost): void {
    const context = host.switchToHttp();
    const request = context.getRequest<Request>();
    const response = context.getResponse<Response<ErrorDto>>();

    response.json({
      timestamp: new Date().toISOString(),
      path: request.url,
      statusCode: exception.getStatus(),
      code: exception.options.code,
      message: exception.options.message,
      details: exception.options.details,
    });

    const error = exception.options.originalError;
    this.logger.error(error?.stack || error);
  }
}
