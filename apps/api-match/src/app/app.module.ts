import { Module, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { WinstonModule } from 'nest-winston';
import { winstonConfig } from './common/logging.config';
import { mongoDbUri } from './database.util';

import { environment } from '../environments/environment';
import { MatchModule } from './match/match.module';
import { TeamModule } from './team/team.module';
import { APP_FILTER, APP_PIPE } from '@nestjs/core';
import { ApiExceptionFilter, ErrorFilter, NotFoundExceptionFilter } from './api-exception-filter';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: environment.envFilePath,
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: mongoDbUri,
      inject: [ConfigService],
    }),
    WinstonModule.forRoot(winstonConfig),
    TeamModule,
    MatchModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    {
      provide: APP_FILTER,
      useClass: ErrorFilter
    },
    {
      provide: APP_FILTER,
      useClass: NotFoundExceptionFilter
    },
    {
      provide: APP_FILTER,
      useClass: ApiExceptionFilter
    }
  ],

})
export class AppModule {}
