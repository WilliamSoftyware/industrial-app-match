export interface TeamDto {
  id: string;
  name: string;
}

// type with all properties from TeamDto, excluding id
export type TeamCreateDto = Omit<TeamDto, 'id'>;

// type with only id property from TeamDto + all properties as optional from TeamCreateDto
export type TeamUpdateDto = Pick<TeamDto, 'id'> & Partial<TeamCreateDto>;
export type TeamResetDto = TeamUpdateDto;
